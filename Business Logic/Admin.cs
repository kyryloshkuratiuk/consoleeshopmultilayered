﻿namespace Business_Logic
{
    public class Admin : RegisteredAccount
    {
        public Admin(string login, string password, string email) : base(login, password, email, EntitieRole.Admin) { }
    }
}
