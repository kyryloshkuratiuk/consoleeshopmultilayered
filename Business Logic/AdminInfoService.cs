﻿using System;

namespace Business_Logic
{
    public class AdminInfoService
    {
        readonly RegisteredAccountService userDataBase;

        public AdminInfoService(RegisteredAccountService userService)
        {
            userDataBase = userService;
        }

        /// <summary>
        /// Gets personal data of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public string GetPersonalData(string login)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            return user.ToString();
        }

        /// <summary>
        /// Changes the login of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newLogin">New login for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangeLogin(string login, string newLogin)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            user.SetLogin(newLogin, userDataBase);
        }

        /// <summary>
        /// Changes the email of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newEmail">New email for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangeEmail(string login, string newEmail)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            user.SetEmail(newEmail, userDataBase);
        }

        /// <summary>
        /// Changes the password of provided user
        /// </summary>
        /// <param name="login">Login of needed user</param>
        /// <param name="newPassword">New password for user</param>
        /// <exception cref="ArgumentException">Thrown if user not found</exception>
        public void ChangePassword(string login, string newPassword)
        {
            User user = userDataBase.GetUserByLogin(login) as User;
            if (user == null) throw new ArgumentException("User not found");
            user.SetPassword(newPassword);
        }
    }
}
