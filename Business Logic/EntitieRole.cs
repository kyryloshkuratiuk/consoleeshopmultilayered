﻿namespace Business_Logic
{
    /// <summary>
    /// Enumeration registered account's available roles
    /// </summary>
    public enum EntitieRole
    {
        User,
        Admin
    }
}
