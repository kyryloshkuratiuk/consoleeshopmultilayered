﻿using System;
using System.Collections.Generic;
using Data_Access;

namespace Business_Logic
{
    public class OrderService
    {
        readonly DataContext<Order> _orders = new DataContext<Order>();

        /// <summary>
        /// Creates new order and returns its ID
        /// </summary>
        public int Create()
        {
            int id = _orders.Get().Count;
            List<Product> products = new List<Product>();
            _orders.Add(new Order(id, products));
            return id;
        }

        /// <summary>
        /// Sets order status to removed
        /// </summary>
        /// <param name="orderId">ID of needed order</param>
        public void Delete(int orderId)
        {
            foreach (Order order in _orders.Get())
            {
                if (order == GetOrderById(orderId))
                {
                    if (order.Status == "Received" || order.Status == "Finished" || order.Status == "Canceled by user") throw new ArgumentException("This order can not be canceled");
                    order.Cancel();
                }
            }
        }

        /// <summary>
        /// Returns object of the order by ID
        /// </summary>
        /// <param name="id">ID of needed order</param>
        public Order GetOrderById(int id)
        {
            foreach (Order order in _orders.Get())
            {
                if (order.Id == id) return order;
            }
            return null;
        }
    }
}
