﻿using System.Collections.Generic;
using Data_Access;

namespace Business_Logic
{
    public class ProductService
    {
        readonly DataContext<Product> _products = new DataContext<Product>();

        /// <summary>
        /// Adds new product to database of products
        /// </summary>
        /// <param name="id">ID for new product</param>
        /// <param name="name">Name for new product</param>
        /// <param name="category">Category for new product</param>
        /// <param name="description">Description for new product</param>
        /// <param name="price">Price for new product</param>
        public void Add(int id, string name, string category, string description, decimal price)
        {
            _products.Add(new Product(id, name, category, description, price));
        }

        /// <summary>
        /// Removes product from database of products by ID
        /// </summary>
        /// <param name="id">ID of product</param>
        public void Remove(int id)
        {
            _products.Remove(GetProductById(id));
        }

        /// <summary>
        /// Returns product by ID
        /// </summary>
        /// <param name="id">ID of product</param>
        public Product GetProductById(int id)
        {
            foreach (Product product in _products.Get())
            {
                if (product.Id == id) return product;
            }
            return null;
        }

        /// <summary>
        /// Returns list of products that include provided name
        /// </summary>
        /// <param name="productName">Name of needed product</param>
        public List<Product> Find(string productName)
        {
            List<Product> result = new List<Product>();
            foreach (Product product in _products.Get())
            {
                if (product.Name.Contains(productName))
                {
                    result.Add(product);
                }
            }
            return result;
        }

        /// <summary>
        /// Returns list of products
        /// </summary>
        public List<Product> GetProducts()
        {
            return _products.Get();
        }
    }
}
