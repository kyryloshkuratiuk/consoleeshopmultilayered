﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Business_Logic
{
    public abstract class RegisteredAccount
    {
        public string Login { get; private set; }
        public string Password { get; private set; }
        public string Email { get; private set; }
        public decimal Balance { get; private set; }
        public EntitieRole Role { get; private set; }
        public List<Order> Orders { get; private set; }

        protected RegisteredAccount(string login, string password, string email, EntitieRole role)
        {
            Login = login;
            Password = password;
            Email = email;
            Role = role;
            Orders = new List<Order>();
        }

        /// <summary>
        /// Changes login of current registered account to new
        /// </summary>
        /// <param name="newLogin">New login for user</param>
        /// <param name="userDataBase">Object of registered account service</param>
        public void SetLogin(string newLogin, RegisteredAccountService userDataBase)
        {
            string loginRegex = "^[a-zA-Z0-9]*$";
            if (!Regex.IsMatch(newLogin, loginRegex) || userDataBase.ContainsLogin(newLogin)) throw new ArgumentException("Provided login is unavailable");
            Login = newLogin;
        }

        /// <summary>
        /// Changes email of current registered account to new
        /// </summary>
        /// <param name="newEmail">New login for user</param>
        /// <param name="userDataBase">Object of registered account service</param>
        public void SetEmail(string newEmail, RegisteredAccountService userDataBase)
        {
            string emailRegex = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$";
            if (!Regex.IsMatch(newEmail, emailRegex) || userDataBase.ContainsEmail(newEmail)) throw new ArgumentException("Provided email is unavailable");
            Email = newEmail;
        }

        /// <summary>
        /// Changes password of current registered account to new
        /// </summary>
        /// <param name="newPassword">New login for user</param>
        public void SetPassword(string newPassword)
        {
            Password = newPassword;
        }

        /// <summary>
        /// Adds new order to current registered accounts's list of orders
        /// </summary>
        /// <param name="order">Object of new order</param>
        public void AddOrder(Order order)
        {
            Orders.Add(order);
        }

        /// <summary>
        /// Adds needed amount to current registered account's balance
        /// </summary>
        /// <param name="amount">Amount to add</param>
        public void AddBalance(decimal amount)
        {
            Balance += amount;
        }

        /// <summary>
        /// Substracts needed amount from current registered account's balance
        /// </summary>
        /// <param name="amount">Amount to substract</param>
        public void SubstractBalance(decimal ammount)
        {
            Balance -= ammount;
        }

        /// <summary>
        /// Returns list of current registered account's orders
        /// </summary>
        public List<Order> GetOrders()
        {
            return Orders;
        }

        /// <summary>
        /// Returns product as a string
        /// </summary>
        public override string ToString()
        {
            return "Login: " + Login + "\nPassword: " + Password + "\nEmail: " + Email + "\nBalance: " + Balance + "\nStatus: " + Role;
        }
    }
}
