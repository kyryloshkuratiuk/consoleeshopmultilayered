﻿namespace Business_Logic
{
    public class User : RegisteredAccount
    {
        public User(string login, string password, string email) : base(login, password, email, EntitieRole.User) { }
    }
}
