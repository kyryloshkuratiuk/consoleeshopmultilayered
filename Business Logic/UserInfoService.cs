﻿namespace Business_Logic
{
    public class UserInfoService
    {
        protected RegisteredAccountService userDataBase;
        protected RegisteredAccount authorizedUser;

        public UserInfoService(RegisteredAccount user, RegisteredAccountService userService)
        {
            userDataBase = userService;
            authorizedUser = user;
        }

        /// <summary>
        /// Changes the login of current user
        /// </summary>
        /// <param name="newLogin">New login for user</param>
        public void ChangeLogin(string newLogin)
        {
            authorizedUser.SetLogin(newLogin, userDataBase);
        }

        /// <summary>
        /// Changes the email of current user
        /// </summary>
        /// <param name="newEmail">New email for user</param>
        public void ChangeEmail(string newEmail)
        {
            authorizedUser.SetEmail(newEmail, userDataBase);
        }

        /// <summary>
        /// Changes the password of current user
        /// </summary>
        /// <param name="newPassword">New password for user</param>
        public void ChangePassword(string newPassword)
        {
            authorizedUser.SetPassword(newPassword);
        }

        /// <summary>
        /// Adds needed amount to current registered account's balance
        /// </summary>
        /// <param name="amount">Amount to add</param>
        public void Deposit(decimal amount)
        {
            authorizedUser.AddBalance(amount);
        }
    }
}
