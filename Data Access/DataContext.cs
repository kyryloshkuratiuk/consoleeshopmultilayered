﻿using System.Collections.Generic;

namespace Data_Access
{
    public class DataContext<T>
    {
        readonly List<T> _list = new List<T>();

        /// <summary>
        /// Adds object of type T to the database
        /// </summary>
        /// <param name="item">Object to add</param>
        public void Add(T item) { _list.Add(item); }

        /// <summary>
        /// Removes object of type T from the database
        /// </summary>
        /// <param name="item">Object to add</param>
        public void Remove(T item) { _list.Remove(item); }

        /// <summary>
        /// Returns list of objects of T type
        /// </summary>
        public List<T> Get() { return _list; }
    }
}
