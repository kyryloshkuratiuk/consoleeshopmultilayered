using NUnit.Framework;
using Data_Access;
using System.Collections.Generic;

namespace WebStore.Tests
{
    public class DataAccessTests
    {
        DataContext<int> dataBase;

        [Test]
        public void Add_AddItemToDataContext_ReturnsCorrectLength()
        {
            // Arrange
            dataBase = new DataContext<int>();
            List<int> newList = new List<int>();
            dataBase.Add(1);
            newList.Add(1);
            int expectedLength = newList.Count;
            
            // Act
            int actualLength = dataBase.Get().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test]
        public void Remove_RemoveItemFromDataContext_ReturnsCorrectLength()
        {
            // Arrange
            dataBase = new DataContext<int>();
            dataBase.Add(1);
            dataBase.Remove(1);
            int expectedLength = 0;

            // Act
            int actualLength = dataBase.Get().Count;

            // Assert
            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test]
        public void Get_ReturnsListOfExistingT()
        {
            // Arrange
            dataBase = new DataContext<int>();
            dataBase.Add(1);
            List<int> expectedList = new List<int>();
            expectedList.Add(1);

            // Act
            List<int> actualList = dataBase.Get();

            // Assert
            CollectionAssert.AreEqual(expectedList, actualList);
        }
    }
}