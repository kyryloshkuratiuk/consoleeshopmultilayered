﻿using Presentation;
using Business_Logic;

namespace WebStore
{
    class Program
    {
        static void Main(string[] args)
        {
            User currentUser = null;
            RegisteredAccountService userDataBase = new RegisteredAccountService();
            OrderService orderDataBase = new OrderService();
            ProductService productDataBase = new ProductService();
            Admin admin = new Admin("kirill", "6813261", "kirillshkuratiuk@gmail.com"); // Adding elements to the imaginary database
            User user = new User("pitbull", "1337228", "iloveyou@and.me");
            productDataBase.Add(4, "Samsung Galaxy S8 64GB", "Phones", "Smartphone", 14999);
            productDataBase.Add(5, "Samsung Note 9 128GB", "Phones", "Smartphone", 34999);
            productDataBase.Add(6, "Apple Watch Series 5 44mm", "Watches", "Smartwatch", 14999);
            userDataBase.Add(admin);
            userDataBase.Add(user);
            Menu menu = new Menu(currentUser, userDataBase, orderDataBase, productDataBase);
            menu.Start();
        }
    }
}
